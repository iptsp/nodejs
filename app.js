let port = 3000;
const express      = require('express');
const bodyParser   = require('body-parser');
const userRoute    = require('./routes/user.route');
const packageRoute = require('./routes/package.route');
const clientRoute  = require('./routes/client.route');

const app    = express();

const mongoose = require('mongoose');
let dev_db_url = 'mongodb://127.0.0.1:27017/cdr';

const mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error',
    console.error.bind(console, 'MongoDB connection error:')
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/api/users', userRoute);
app.use('/api/packages', packageRoute);
app.use('/api/clients', clientRoute);

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: err
    });
});

app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: err
    });
});

app.listen(port, () => console.log('Node server running on port '+port+'!'));