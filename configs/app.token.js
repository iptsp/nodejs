const jwt    = require('jsonwebtoken');

exports.verifyToken = (req, res, next) => {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        req.token = bearer[1];
        jwt.verify(req.token, "keys", (err) => {
            if(err) {
                res.sendStatus(400);
            } else {
                let decoded = jwt.decode(req.token);
                req.user = decoded.userInfo;
                next();
            }
        });
    } else {
        res.sendStatus(403);
    }
};

exports.generateToken = (user) => {
    return jwt.sign({ userInfo: user }, "keys", { expiresIn: '2 days' });
};