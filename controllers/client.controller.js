const ClientModel = require('../models/client.model');
const tokenConfig = require('../configs/app.token');

exports.create = function (req, res, next) {
    let client = new ClientModel(
        {
            name: req.body.name,
            number: req.body.number,
            type: req.body.type,
            package_id: req.body.package_id,
            mobile: req.body.mobile,
            email: req.body.email,
            extension: req.body.extension
        }
    );
    client.save(function (err, client) {
        if (err) {
            return next(err);
        }
        res.json({
            status: true,
            message: "Client Created successfully",
            data: client,
            token: tokenConfig.generateToken(req.uid)
        });
    });
};

exports.all = function (req, res, next) {
    ClientModel.aggregate({
        "$lookup": {
            "localField": "package_id",
            "from": "packages",
            "foreignField": "_id",
            "as": "packInfo"
        }
    }, function (err, clients) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "Data retrieved",
            data: clients
        });
    });
};

exports.delete = function (req, res, next) {
    ClientModel.findOneAndDelete(req.params.id, function (err) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "Deleted successfully!",
            data: null,
            token: tokenConfig.generateToken(req.uid)
        });
    });
};

exports.update = function (req, res, next) {
    ClientModel.findOneAndUpdate(req.params.id, {$set: req.body}, function (err, client) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "User updated.",
            data: client,
            token: tokenConfig.generateToken(req.uid)
        });
    });
};

exports.info = function (req, res, next) {
    res.send(req.params.id);
};

