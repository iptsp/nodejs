const PackageModel = require('../models/package.model');
const tokenConfig = require('../configs/app.token');

exports.create = function (req, res, next) {
    let pack = new PackageModel(
        {
            name: req.body.name,
            tariff: req.body.tariff,
            status: req.body.status
        }
    );
    pack.save(function (err, pack) {
        if (err) {
            return next(err);
        }
        res.json({
            status: true,
            message: "Package Created successfully",
            data: pack,
            token: tokenConfig.generateToken(req.uid)
        });
    });
};
exports.all = function (req, res, next) {
    PackageModel.find(function (err, pack) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "Data retrieved",
            data: pack
        });
    });
};
exports.delete = function (req, res, next) {
    PackageModel.findOneAndDelete(req.params.id, function (err) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "Deleted successfully!",
            data: null,
            token: tokenConfig.generateToken(req.uid)
        });
    });
};
exports.update = function (req, res, next) {
    PackageModel.findOneAndUpdate(req.params.id, {$set: req.body}, function (err, pack) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "Product updated.",
            data: pack,
            token: tokenConfig.generateToken(req.uid)
        });
    });
};