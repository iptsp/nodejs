const UserModel = require('../models/users.model');
const tokenConfig = require('../configs/app.token');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;

exports.login = function (req, res) {
    let un = req.body.username;
    let pw = req.body.password;
    let _req = req;
    let _res = res;

    if(!un || !pw) {
        _res.sendStatus(401);
    }

    UserModel.findOne(
        {
            username: un
        },
        function (err, user) {
            if(err || !user) {
                _res.sendStatus(401);
            } else {
                bcrypt.compare(pw, user.password, function(err, res) {
                    if(err || !res) {
                        _res.sendStatus(401);
                    }
                    jwt.sign({ userInfo: user }, "keys", { expiresIn: '2 days' }, function(err, token) {
                        if(err) {
                            _res.sendStatus(408);
                        } else if(!user) {
                            _res.sendStatus(404);
                        } else {
                            _res.json({
                                status: true,
                                message: "User logged in.",
                                data: user,
                                token: token
                            });
                        }
                    });
                });
            }
        }
    );
};

exports.create = function(req, res, next) {
   let pw = req.body.password;
    bcrypt.hash(pw, saltRounds, function(err, hash) {
        if (err) {
            res.sendStatus(401);
        }
        let userModel = new UserModel(
            {
                username: req.body.username,
                password: hash,
                role: req.body.role,
                name: req.body.name,
                mobile: req.body.mobile,
                email: req.body.email,
                extension: req.body.extension
            }
        );
        userModel.save(function (err, client) {
            if (err) {
                return next(err);
            }
            res.json({
                status: true,
                message: "User Created successfully",
                data: client,
                token: tokenConfig.generateToken(client)
            });
        });
    });
};

exports.all = function (req, res, next) {
    UserModel.find(function (err, users) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "Data retrieved",
            data: users
        });
    });
};

exports.delete = function (req, res, next) {
    UserModel.findOneAndDelete(req.params.id, function (err) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "Deleted successfully!",
            data: null,
            token: tokenConfig.generateToken(req.uid)
        });
    });
};

exports.update = function (req, res, next) {
    UserModel.findOneAndUpdate(req.params.id, {$set: req.body}, function (err, user) {
        if (err) return next(err);
        res.json({
            status: true,
            message: "User updated.",
            data: user,
            token: tokenConfig.generateToken(req.uid)
        });
    });
};

exports.password = function (req, res, next) {
    res.send(req.params.id);
};

exports.info = function (req, res, next) {
    UserModel.findOne(req.params.id, function (err, user) {
        if (err) return next(err);
        res.status(200).json({
            status: true,
            message: "Data retrieved",
            data: user
        });
    });
};