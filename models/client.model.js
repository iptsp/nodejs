const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ClientSchema = new Schema({
    name: {type: String, required: true, max: 15},
    number: {type: String, required: true, max: 31},
    type: {type: String, required: true, max: 15},
    package_id: {type: mongoose.Schema.Types.ObjectId, ref: "Packages", required: true, max: 15},
    mobile: {type: String, required: true, max: 11},
    email: {type: String, required: false, max: 63},
    extension: {type: String, required: false, max: 255}
});

module.exports = mongoose.model('Clients', ClientSchema);