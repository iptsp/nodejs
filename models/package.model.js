const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PackageSchema = new Schema({
    name: {type: String, required: true, max: 15},
    tariff: {type: Number, required: true, max: 6},
    status: {type: String, required: true, max: 15}
});

module.exports = mongoose.model('Packages', PackageSchema);