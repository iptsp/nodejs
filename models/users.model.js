const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    username: {type: String, required: true, unique: true, max: 15},
    password: {type: String, required: true, max: 100},
    role: {type: String, required: true, max: 15},
    name: {type: String, required: true, max: 63},
    mobile: {type: String, required: false, max: 11},
    email: {type: String, required: false, max: 63}
});

module.exports = mongoose.model('Users', UserSchema);