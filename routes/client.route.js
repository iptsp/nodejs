const express = require('express');
const tokenConfig = require('../configs/app.token');
const router = express.Router();

const clientController = require('../controllers/client.controller');

router.get('/', tokenConfig.verifyToken, clientController.all);
router.post('/create', tokenConfig.verifyToken, clientController.create);
router.get('/info/:id', tokenConfig.verifyToken, clientController.info);
router.put('/update/:id', tokenConfig.verifyToken, clientController.update);
router.delete('/delete/:id', tokenConfig.verifyToken, clientController.delete);

module.exports = router;