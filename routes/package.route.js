const express = require('express');
const tokenConfig = require('../configs/app.token');
const router = express.Router();

const packageController = require('../controllers/package.controller');

router.get('/', tokenConfig.verifyToken, packageController.all);
router.post('/create', tokenConfig.verifyToken, packageController.create);
router.put('/update/:id', tokenConfig.verifyToken, packageController.update);
router.delete('/delete/:id', tokenConfig.verifyToken, packageController.delete);

module.exports = router;