const express = require('express');
const tokenConfig = require('../configs/app.token');
const router = express.Router();

const userController = require('../controllers/user.controller');

router.post('/login', userController.login);

router.get('/', tokenConfig.verifyToken, userController.all);
router.delete('/delete/:id', tokenConfig.verifyToken, userController.delete);
router.put('/update/:id', tokenConfig.verifyToken, userController.update);
router.post('/password/:id', tokenConfig.verifyToken, userController.password);
router.post('/create', tokenConfig.verifyToken, userController.create);
router.get('/info/:id', tokenConfig.verifyToken, userController.info);

module.exports = router;